# P5JS
This is a test project. I try to create a P5JS app as a NFT on IPFS.
I followed the [tutorial](https://docs.tableland.xyz/creating-a-dynamic-nft-with-p5js-and-tableland) from the platform Tableland
And upload the generated project on [NFT.storage](https://nft.storage)

# Installation
First execute *install_node_dependencies.sh* to install the requirements for the project.  

# Testing
`npm run start` to start the app on `localhost:8080`

# Build
Change "mode" from "development" to "production" in *webpack.config.js*  
```
module.exports = {
    mode: "production",
[...]
```

# Upload project to NFT.storage
I converted the project to a CAR (Content archive format), as explained [here](https://nft.storage/docs/concepts/car-files/)
1. If not already done, first install ipfs-car globally, so that it is included in PATH `npm install -g ipfs-car`  
2. Create a folder named *./ipfs* (name is not important) `mkdir ./ipfs`   
3. `ipfs-car --pack ./dist --output ./ipfs` 
You should see the output  
```
ipfs-car --pack ./dist --output ./ipfs/p5js.car
root CID: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
  output: ./ipfs/p5js.car
```  
If everything went well, you can now upload the output file from *./ipfs/p5js.car* to [NFT.storage](https://nft.storage/files/)  
You will need a HTTP gateway if you want to see the app display in your web browser, without a IPFS client. I chose [https://cloudflare-ipfs.com](https://cloudflare-ipfs.com) as the gateway from my project.  
